#include "Backend.h"

#include "QDebug"
#include <chrono>

Backend::Backend(QObject *parent)
    : QObject{parent}
    , _participant(nullptr)
    , _publisher(nullptr)
    , _deltaMs(0)
    , _receivedTimestamp(0)
{
    _message.timestamp(0);
    _message.message("Not set");

    DomainParticipantQos participantQos;
    participantQos.name("Participant_publisher");
    _participant = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);

    if (_participant != nullptr)
    {
        // Create publisher and subscriber
        _publisher = new DdsPublisher<RoundTripPubSubType>(_participant, "RoundTripSend");
        _subscriber = new DdsSubscriber<RoundTripPubSubType>(_participant, "RoundTripReceive");
    }

    _publisher->bindPublicationMatchedCallback(this, &Backend::_onPublicationMatched);

    _subscriber->bindSubscriptionMatchedCallback(this, &Backend::_onSubscriptionMatched);
    _subscriber->bindDataAvailableCallback(this, &Backend::_onDataAvailable);
}

Backend::~Backend()
{
    delete _publisher;
    _publisher = nullptr;
    DomainParticipantFactory::get_instance()->delete_participant(_participant);
    _participant = nullptr;
}

QString Backend::message() const
{
    return {_message.message().c_str()};
}

void Backend::setMessage(QString message)
{
    std::string msg = message.toStdString();

    if (msg == _message.message())
        return;

    _message.message(msg);

    emit messageChanged(message);
}

void Backend::send()
{
    auto time = std::chrono::system_clock::now().time_since_epoch();
    _message.timestamp(std::chrono::duration_cast<std::chrono::nanoseconds>(time).count());

    _publisher->publish(_message);

    emit sendTimestampChanged();
}

QDateTime Backend::sendTimestamp() const
{
    return QDateTime::fromMSecsSinceEpoch(_message.timestamp()/1000000);
}

int Backend::subscriberCount() const
{
    if (!_publisher)
        return 0;

    return _publisher->subscriberCount();
}

bool Backend::connected() const
{
    if (!_subscriber)
        return false;

    return _subscriber->connected();
}

void Backend::_onPublicationMatched(const PublicationMatchedStatus &info)
{
    emit subscriberCountChanged(_publisher->subscriberCount());
}

void Backend::_onSubscriptionMatched(const SubscriptionMatchedStatus &info)
{
    emit connectedChanged(_subscriber->connected());
}

void Backend::_onDataAvailable(const RoundTrip &message, const SampleInfo &info)
{
    auto time = std::chrono::system_clock::now().time_since_epoch();
    _receivedTimestamp = std::chrono::duration_cast<std::chrono::nanoseconds>(time).count();

    _deltaMs = (_receivedTimestamp - message.timestamp())/1000000;

    emit dataReceived();
}

QDateTime Backend::receiveTimestamp() const
{
    return QDateTime::fromMSecsSinceEpoch(_receivedTimestamp/1000000);
}

int Backend::delta() const
{
    return _deltaMs;
}
