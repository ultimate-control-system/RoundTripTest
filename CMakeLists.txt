cmake_minimum_required(VERSION 3.14)

project(RoundTripTest VERSION 0.1 LANGUAGES CXX)

# DDS Library
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../library ${CMAKE_CURRENT_SOURCE_DIR}/library)

add_subdirectory(Sender)
add_subdirectory(Receiver)

