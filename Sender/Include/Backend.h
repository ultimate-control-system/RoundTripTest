#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QDateTime>

#include "DdsPublisher.hpp"
#include "DdsSubscriber.hpp"
#include "RoundTrip/RoundTripPubSubTypes.h"

class Backend : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString message READ message WRITE setMessage NOTIFY messageChanged)
    Q_PROPERTY(QDateTime sendTimestamp READ sendTimestamp NOTIFY sendTimestampChanged)
    Q_PROPERTY(QDateTime receiveTimestamp READ receiveTimestamp NOTIFY dataReceived)
    Q_PROPERTY(int delta READ delta NOTIFY dataReceived)
    Q_PROPERTY(int subscriberCount READ subscriberCount NOTIFY subscriberCountChanged)
    Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged)

public:
    explicit Backend(QObject *parent = nullptr);

    ~Backend();

    QString message() const;
    void setMessage(QString message);

    QDateTime sendTimestamp() const;
    QDateTime receiveTimestamp() const;
    int delta() const;

    int subscriberCount() const;

    bool connected() const;


    Q_INVOKABLE
    void send();

signals:
    void messageChanged(QString);
    void sendTimestampChanged();
    void subscriberCountChanged(int);
    void connectedChanged(bool);
    void dataReceived();

private:
    DomainParticipant* _participant;
    DdsPublisher<RoundTripPubSubType>* _publisher;
    DdsSubscriber<RoundTripPubSubType>* _subscriber;

    RoundTrip _message;
    int _deltaMs;
    uint64_t _receivedTimestamp;

    void _onPublicationMatched(const PublicationMatchedStatus& info);
    void _onSubscriptionMatched(const SubscriptionMatchedStatus& info);
    void _onDataAvailable(const RoundTrip& message, const SampleInfo& info);

};

#endif // BACKEND_H
