#include "Backend.h"

Backend::Backend(QObject *parent)
    : QObject{parent}
    , _participant(nullptr)
    , _subscriber(nullptr)
    , _publisher(nullptr)
    , _subscriberCount(0)
{
    _receivedMessage.timestamp(0);
    _receivedMessage.message("Not set");

    DomainParticipantQos participantQos;
    participantQos.name("Receiver participant");
    _participant = DomainParticipantFactory::get_instance()->create_participant(0, participantQos);

    if (_participant == nullptr)
        return;

    // Create publisher and subscriber
    _subscriber = new DdsSubscriber<RoundTripPubSubType>(_participant, "RoundTripSend");
    _publisher = new DdsPublisher<RoundTripPubSubType>(_participant, "RoundTripReceive");

    _subscriber->bindDataAvailableCallback(this, &Backend::_dataAvailable);
    _subscriber->bindSubscriptionMatchedCallback(this, &Backend::_subscriptionMatched);

    _publisher->bindPublicationMatchedCallback(this, &Backend::_publicationMatched);

}

Backend::~Backend()
{
    delete _subscriber;
    _subscriber = nullptr;
    DomainParticipantFactory::get_instance()->delete_participant(_participant);
    _participant = nullptr;
}

QString Backend::message() const
{
    return {_receivedMessage.message().c_str()};
}

QDateTime Backend::timestamp() const
{
    return QDateTime::fromMSecsSinceEpoch(_receivedMessage.timestamp()/1000000);
}

void Backend::_dataAvailable(const RoundTrip &msg, const SampleInfo &info)
{
    _receivedMessage = msg;
    _publisher->publish(msg);
    emit messageChanged();
}

bool Backend::connected() const
{
    if (!_subscriber)
        return false;

    return _subscriber->connected();
}

int Backend::subscriberCount() const
{
    if (!_publisher)
        return 0;

    return _publisher->subscriberCount();
}

void Backend::_subscriptionMatched(const SubscriptionMatchedStatus&)
{
    emit connectedChanged(_subscriber->connected());
}

void Backend::_publicationMatched(const PublicationMatchedStatus &info)
{
    emit subscriberCountChanged(_publisher->subscriberCount());
}
