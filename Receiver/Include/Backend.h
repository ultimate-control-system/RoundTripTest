#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QDateTime>

#include "DdsSubscriber.hpp"
#include "DdsPublisher.hpp"
#include "RoundTrip/RoundTripPubSubTypes.h"

class Backend : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString message READ message NOTIFY messageChanged)
    Q_PROPERTY(QDateTime timestamp READ timestamp NOTIFY messageChanged)
    Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged)
    Q_PROPERTY(int subscriberCount READ subscriberCount NOTIFY subscriberCountChanged)

public:
    explicit Backend(QObject *parent = nullptr);

    ~Backend();

    QString message() const;
    QDateTime timestamp() const;

    bool connected() const;
    int subscriberCount() const;

signals:
    void messageChanged();
    void connectedChanged(bool);
    void subscriberCountChanged(int);

private:
    DomainParticipant* _participant;
    DdsSubscriber<RoundTripPubSubType>* _subscriber;
    DdsPublisher<RoundTripPubSubType>* _publisher;

    RoundTrip _receivedMessage;
    int _subscriberCount;

    void _dataAvailable(const RoundTrip& msg, const SampleInfo& info);
    void _subscriptionMatched(const SubscriptionMatchedStatus& info);
    void _publicationMatched(const PublicationMatchedStatus& info);

};

#endif // BACKEND_H
