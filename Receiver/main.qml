import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

import BackendLib 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Round Trip Test Receiver")

    ReceiverBackend {
        id: backend
    }

    Row {
        spacing: 10
        padding: 10

        TextField {
            text: backend.message
            enabled: false
        }

        TextField {
            text: backend.timestamp
            enabled: false
            width: 200
        }

        TextField {
            text: backend.connected ? qsTr("Connected") : qsTr("Disconnected")
            enabled: false
        }

        TextField {
            text: qsTr("Subscribers: " + backend.subscriberCount);
            enabled: false
        }
    }
}
