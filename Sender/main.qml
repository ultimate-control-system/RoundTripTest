import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

import BackendLib 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Round Trip Test Sender")

    SenderBackend {
        id: backend
    }

    Column {
        spacing: 10

        Row {
            spacing: 10
            padding: 10

            TextField {
                text: backend.message
                onActiveFocusChanged: {
                    if(activeFocus)
                        selectAll();
                    else
                        backend.message = text
                }
                onAccepted: { focus = false }
            }

            TextField {
                text: backend.sendTimestamp
                enabled: false
                width: 200
            }

            TextField {
                text: backend.connected ? qsTr("Connected") : qsTr("Disconnected")
                enabled: false
            }

            TextField {
                text: qsTr("Subscribers: ") + backend.subscriberCount
                enabled: false
            }
        }

        Row {
            spacing: 10
            padding: 10
            Button {
                text: qsTr("Send")
                onClicked: backend.send()
            }

            TextField {
                text: backend.receiveTimestamp
                enabled: false
                width: 200
            }

            TextField {
                text: backend.delta
                enabled: false
                width: 200
            }
        }
    }
}

